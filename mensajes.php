<?php
use Symfony\Component\HttpFoundation\Request;
$app->get('/api/temas/{idTema}/mensajes/{pagina}/{cantElement}', function ($idTema, $pagina, $cantElement) use ($app) {
    $mensajeController = new MensajeController();
    return $app->json($mensajeController->getMensajePorIdTema($idTema, $pagina, $cantElement));
});

$app->post('/api/mensajes/', function (Request $request) use ($app) {
    $mensajeController = new MensajeController();
    return $app->json($mensajeController->enviarMensaje($request->request->get('contenido'),$request->request->get('idTema'),
        $request->request->get('nick'),$app['session']));
});