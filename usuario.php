<?php
use Symfony\Component\HttpFoundation\Request;
use Silex\Provider\CsrfServiceProvider;

//$app->register(new CsrfServiceProvider());

$app->post('/api/usuario/login', function (Request $request) use ($app) {
    $sesionController = new SesionController();
    return $app->json($sesionController->
    crearSesion($request->request->get('nick'), $request->request->get('pass'), $app['session']));
});

$app->get('/api/usuario/logout', function () use ($app) {
    $sesionController = new SesionController();
    return $app->json($sesionController->cerrarSesion($app['session']));
});

$app->post('/api/usuario/', function (Request $request) use ($app) {
    $usuarioController = new UsuarioController();
    return $app->json($usuarioController->registrarUsuario($request->request->get('nick'),
        $request->request->get('nombre'), $request->request->get('apellido'), $request->request->get('email'),
        $request->request->get('pass')));
});