<?php

class TemaDao implements PatronTemaDao
{

    /**
     * @param $nombre
     * @return mixed
     */
    function obtenerCantTemasPorNombreForo($nombre)
    {
        $bd = new BDobject();
        return $bd->ejecutarConsulta("SELECT count(0) 
                                      as cantidad
                                      FROM tema t
                                      JOIN foro f 
                                      on t.foro_idforo = f.idforo
                                      WHERE f.nombre = '$nombre';")[0]["cantidad"];
    }

    /**
     * @param $nombre
     * @param $desde
     * @param $cantidad
     * @return mixed
     */
    public function obtenerTemasPorNombreForo($nombre, $desde, $cantidad)
    {
        $bd = new BDobject();
        return $bd->ejecutarConsulta("SELECT t.idtema,
                                      t.nombre as nombreTema,
                                      u.nick as nickUsuario,
                                      t.fecha as fechaTema,
                                      count(m.idmensaje) as respuestas,
                                     (SELECT mensaje.fecha
                                      from mensaje
                                      join tema 
                                      on mensaje.tema_idtema = tema.idtema
                                      WHERE tema.idtema = t.idtema
                                      ORDER BY mensaje.fecha
                                      desc LIMIT 1) 
                                      as fechaUltimoMensaje,
                                     (SELECT usuario.nick
                                      from mensaje
                                      join tema 
                                      on mensaje.tema_idtema = tema.idtema
                                      join usuario 
                                      on mensaje.usuario_idusuario = usuario.idusuario
                                      WHERE tema.idtema = t.idtema
                                      ORDER BY mensaje.fecha
                                      desc LIMIT 1)  
                                      as nickUltimoMensaje
                                      FROM tema t
                                      join foro f 
                                      on t.foro_idforo = f.idforo
                                      join usuario u 
                                      on t.usuario_idusuario = u.idusuario
                                      join mensaje m 
                                      on t.idtema = m.tema_idtema
                                      WHERE f.nombre = '$nombre'
                                      GROUP BY t.idtema ORDER BY fechaUltimoMensaje desc LIMIT $desde,$cantidad;");
    }


    public function crearTema($titulo, $idForo, $idUsuario)
    {
       $bd = new BDobject();
       return $bd->ejecutarInsert("insert into tema VALUE (null,'$titulo',$idForo,$idUsuario,NOW())");
    }
}