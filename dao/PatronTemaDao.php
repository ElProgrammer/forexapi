<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 5/3/2017
 * Time: 4:59 PM
 */
interface PatronTemaDao
{
    public function obtenerCantTemasPorNombreForo($nombre);

    public function obtenerTemasPorNombreForo($nombre, $desde, $cantidad);

    public function crearTema($titulo, $idForo, $idUsuario);
}