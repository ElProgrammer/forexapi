<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 4/4/2017
 * Time: 11:08 PM
 */
interface PatronUsuarioDao
{
    function findByNick($usuario);

    function findByNickYPass($usuario, $pass);

    function crearUsuario($nick, $nombre, $apellido, $email, $pass);
}