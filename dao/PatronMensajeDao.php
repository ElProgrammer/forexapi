<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 5/3/2017
 * Time: 3:36 PM
 */
interface PatronMensajeDao
{
    function obtenerCantMensajesPorNombreForo($nombre);

    function crearMensaje($contenido, $idTema, $idUsuario);

    function getMensajePorIdTema($idTema, $desde, $cantidad);

    function obtenerCantMensajesPorIdTema($idTema);
}