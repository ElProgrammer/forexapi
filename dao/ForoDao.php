<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 5/3/2017
 * Time: 1:13 AM
 */
class ForoDao extends GlobalDao implements PatronForoDao
{

    function buscarPorId($id)
    {
        // TODO: Implement buscarPorId() method.
    }

    function buscarPorNombre($nombre)
    {
        // TODO: Implement buscarPorNombre() method.
    }


    function obtenerTodos()
    {
        $bd = new BDobject();
        return $bd->ejecutarConsulta("SELECT f.idforo as id, f.nombre, c.nombre as categoria
                                      FROM foro f
                                      JOIN categoria c on f.categoria_idcategoria = c.idcategoria;");
    }
}