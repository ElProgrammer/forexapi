<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 5/3/2017
 * Time: 3:38 PM
 */
class MensajeDao implements PatronMensajeDao
{

    function obtenerCantMensajesPorNombreForo($nombre)
    {
        $bd = new BDobject();
        return $bd->ejecutarConsulta("SELECT count(0) as cantidad
                                      FROM mensaje m
                                      JOIN tema t on m.tema_idtema = t.idtema
                                      JOIN foro f on t.foro_idforo = f.idforo
                                      WHERE f.nombre = '$nombre';")[0]["cantidad"];
    }


    function crearMensaje($contenido, $idTema, $idUsuario)
    {
        $bd = new BDobject();
        return $bd->ejecutarInsert("INSERT INTO mensaje VALUE (null,'$contenido',$idTema,$idUsuario,NOW());");
    }

    function getMensajePorIdTema($idTema, $desde, $cantidad)
    {
        $bd = new BDobject();
        return $bd->ejecutarConsulta("SELECT u.nick,u.idusuario,m.contenido,(SELECT count(0) from mensaje
                                             join usuario on mensaje.usuario_idusuario = usuario.idusuario 
                                             WHERE usuario.idusuario = u.idusuario) as cantidad
                                             FROM mensaje m
                                             join usuario u on m.usuario_idusuario = u.idusuario
                                             join tema t on m.tema_idtema = t.idtema
                                             WHERE t.idtema = $idTema ORDER BY m.fecha ASC LIMIT $desde,$cantidad;");
    }

    function obtenerCantMensajesPorIdTema($idTema)
    {
        $bd = new BDobject();
        return $bd->ejecutarConsulta("SELECT count(0) as cantidad
                                             FROM mensaje m
                                             join tema t on m.tema_idtema = t.idtema
                                             WHERE t.idtema = $idTema;")[0]["cantidad"];
    }
}