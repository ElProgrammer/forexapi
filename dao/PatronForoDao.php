<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 5/3/2017
 * Time: 1:14 AM
 */


interface PatronForoDao
{
    function buscarPorNombre($nombre);
    function obtenerTodos();
}