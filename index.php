<?php
ini_set('display_errors', 1);

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

require_once __DIR__ . '/./vendor/autoload.php';
require_once __DIR__ . '/./BDobject.php';
require_once __DIR__ . '/./dao/GlobalDao.php';
require_once __DIR__ . '/./dao/PatronForoDao.php';
require_once __DIR__ . '/./dao/PatronMensajeDao.php';
require_once __DIR__ . '/./dao/PatronTemaDao.php';
require_once __DIR__ . '/./dao/PatronUsuarioDao.php';
require_once __DIR__ . '/./dao/ForoDao.php';
require_once __DIR__ . '/./dao/MensajeDao.php';
require_once __DIR__ . '/./dao/TemaDao.php';
require_once __DIR__ . '/./dao/UsuarioDao.php';
require_once __DIR__ . '/./negocio/ForoController.php';
require_once __DIR__ . '/./negocio/TemaController.php';
require_once __DIR__ . '/./negocio/SesionController.php';
require_once __DIR__ . '/./negocio/MensajeController.php';
require_once __DIR__ . '/./negocio/UsuarioController.php';
require_once __DIR__ . '/./objects/ForoDTO.php';
require_once __DIR__ . '/./objects/CategoriaDTO.php';

$app = new Silex\Application();
$app['debug'] = true;
$app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->headers->set('Access-Control-Allow-Headers', 'Content-Type');
    $response->headers->set('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
});

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

$app->match("{url}", function ($url) use ($app) {
    return "OK";
})->assert('url', '.*')->method("OPTIONS");

$app->register(new Silex\Provider\SessionServiceProvider());

require __DIR__ . '/./foros.php';
require __DIR__ . '/./temas.php';
require __DIR__ . '/./usuario.php';
require __DIR__ . '/./mensajes.php';


$app->run();