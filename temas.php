<?php

use Symfony\Component\HttpFoundation\Request;

$app->get('/api/temas/{nombreForo}/{pagina}/{cantElement}', function ($nombreForo, $pagina, $cantElement) use ($app) {
    $temaController = new TemaController();
    return $app->json($temaController->getTemasPorNombreForo($nombreForo, $pagina, $cantElement));
});

$app->post('/api/temas/', function (Request $request) use ($app) {
    $temaController = new TemaController();
    return $app->json($temaController->
    crearTema($request->request->get('titulo'), $request->request->get('contenido'), $request->request->get('idForo'),
        $request->request->get('nick'), $app['session']));
});