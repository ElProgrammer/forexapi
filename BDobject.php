<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 5/3/2017
 * Time: 1:22 AM
 */
class BDobject
{
    private $servidor = "localhost";
    private $user = "root";
    private $pass = "";
    private $db = "forex";
    private $conn = null;

    private function crearConexion()
    {
        $this->conn = new mysqli($this->servidor, $this->user, $this->pass, $this->db);
        $this->conn->set_charset("utf8");
        if ($this->conn->connect_error) {
            die("Error: " . $this->conn->connect_error);
        }
    }

    public function ejecutarConsulta($query)
    {
        $this->crearConexion();
        $result = $this->conn->query($query);
        $this->conn->close();
        if ($result === false) {
            return [];
        } else {
            return $result->fetch_all(MYSQLI_ASSOC);
        }
    }

    public function ejecutarInsert($query)
    {
        $this->crearConexion();
        $result = $this->conn->query($query);
        if ($result === false) {
            $this->conn->close();
            return array('success' => false);
        } else {
            $id = $this->conn->insert_id;
            $this->conn->close();
            return array('success' => true,
                'id' => $id);
        }
    }
}