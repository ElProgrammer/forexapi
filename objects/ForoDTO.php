<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 5/3/2017
 * Time: 1:58 AM
 */
class ForoDTO implements JsonSerializable
{

    private $id;
    private $nombre;
    private $categoria;
    private $cantidadMensajes = 0;
    private $cantidadTemas = 0;

    public function __construct()
    {
        // ...
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return int
     */
    public function getCantidadMensajes()
    {
        return $this->cantidadMensajes;
    }

    /**
     * @param int $cantidadMensajes
     */
    public function setCantidadMensajes($cantidadMensajes)
    {
        $this->cantidadMensajes = (int)$cantidadMensajes;
    }

    /**
     * @return int
     */
    public function getCantidadTemas()
    {
        return $this->cantidadTemas;
    }

    /**
     * @param int $cantidadTemas
     */
    public function setCantidadTemas($cantidadTemas)
    {
        $this->cantidadTemas = (int)$cantidadTemas;
    }


    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return get_object_vars($this);
    }
}