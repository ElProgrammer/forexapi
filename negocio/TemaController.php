<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 20/3/2017
 * Time: 12:03 AM
 */
class TemaController
{

    function getTemasPorNombreForo($nombre, $pagina, $cantElement)
    {
        $desde = ($pagina * $cantElement) - $cantElement;
        $temaDao = new TemaDao();
        $maxTemas = $temaDao->obtenerCantTemasPorNombreForo($nombre);
        $maxPaginas = (ceil((((float)$maxTemas) / $cantElement)) === 0) ?
            1 : ceil((((float)$maxTemas) / $cantElement));

        return array(
            'pagina' => $pagina,
            'cantElement' => $cantElement,
            'maxElement' => $maxTemas,
            'maxPaginas' => $maxPaginas,
            'data' => $temaDao->obtenerTemasPorNombreForo($nombre, $desde, $cantElement)

        );

    }

    function crearTema($titulo, $contenido, $idForo, $nickUsuario, $session)
    {

        $sesionCtrl = new SesionController();
        $sessionState = $sesionCtrl->consultarSesion($session, $nickUsuario);
        if ($sessionState['success'] === true) {
            $usuarioDao = new UsuarioDao();
            $user = $usuarioDao->findByNickYPass($sessionState['user'], $sessionState['pass'])[0];
            $temaDao = new TemaDao();
            $temaResult = $temaDao->crearTema($titulo, $idForo, $user['idusuario']);
            if ($temaResult['success'] === true) {
                $mensajeDao = new MensajeDao();
                $mensajeResult = $mensajeDao->crearMensaje($this->trimUnicode($contenido), $temaResult['id'], $user['idusuario']);
                if ($mensajeResult['success']) {
                    return array('mensaje' => "Tema creado correctamente",
                        'success' => true);
                } else {
                    return array('mensaje' => "Error creando el mensaje",
                        'success' => false);
                }
            } else {
                return array('mensaje' => "Error creando el tema",
                    'success' => false);
            }
        } else {
            return $sessionState;
        }
        //
    }

    function trimUnicode($str) {
        return preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u','',$str);
    }

}