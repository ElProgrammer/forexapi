<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 8/4/2017
 * Time: 6:38 AM
 */
class UsuarioController
{

    function registrarUsuario($nick, $nombre, $apellido, $email, $pass)
    {

        $usuarioDao = new UsuarioDao();
        $usuario = $usuarioDao->findByNick($nick);
        if (count($usuario) === 0) {

            $resultUsuario = $usuarioDao->crearUsuario($nick, $nombre, $apellido, $email, $pass);

            if ($resultUsuario['success'] === true) {
                return array('mensaje' => "Se a registrado correctamente",
                    'success' => true);
            } else {
                return array('mensaje' => "Error al crear el usuario",
                    'success' => false);
            }

        } else {
            return array('mensaje' => "El usuario ya existe",
                'success' => false);
        }

    }

}