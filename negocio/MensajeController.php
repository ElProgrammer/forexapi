<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 8/4/2017
 * Time: 2:41 AM
 */
class MensajeController
{
    function getMensajePorIdTema($idTema, $pagina, $cantElement)
    {
        $mensajeDao = new MensajeDao();

        $desde = ($pagina * $cantElement) - $cantElement;
        $maxTemas = $mensajeDao->obtenerCantMensajesPorIdTema($idTema);
        $maxPaginas = (ceil((((float)$maxTemas) / $cantElement)) === 0) ?
            1 : ceil((((float)$maxTemas) / $cantElement));

        return array(
            'pagina' => $pagina,
            'cantElement' => $cantElement,
            'maxElement' => $maxTemas,
            'maxPaginas' => $maxPaginas,
            'data' => $mensajeDao->getMensajePorIdTema($idTema, $desde, $cantElement)

        );

    }

    function enviarMensaje($contenido, $idTema, $nickUsuario, $session)
    {
        $sesionCtrl = new SesionController();
        $sessionState = $sesionCtrl->consultarSesion($session, $nickUsuario);
        if ($sessionState['success'] === true) {
            $usuarioDao = new UsuarioDao();
            $user = $usuarioDao->findByNickYPass($sessionState['user'], $sessionState['pass'])[0];
            $mensajeDao = new MensajeDao();
            $mensajeResult = $mensajeDao->crearMensaje($this->trimUnicode($contenido), $idTema, $user['idusuario']);
            if ($mensajeResult['success']) {
                return array('mensaje' => "Mensaje creado correctamente",
                    'success' => true);
            } else {
                return array('mensaje' => "Error creando el mensaje",
                    'success' => false);
            }
        } else {
            return $sessionState;
        }
    }

    function trimUnicode($str) {
        return preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u','',$str);
    }

}