<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 4/4/2017
 * Time: 11:24 PM
 */
class SesionController
{
    function crearSesion($nick, $pass, $session)
    {
        $usuarioDao = new UsuarioDao();
        $usuario = $usuarioDao->findByNickYPass($nick, $pass);
        if (count($usuario) === 0) {
            return array('mensaje' => "El usuario no existe",
                'success' => false);
        } else {
            $this->cerrarSesion($session);
            $session->set('user', $usuario[0]['nick']);
            $session->set('pass', $usuario[0]['pass']);

            return array('mensaje' => "Bienvenido " . $usuario[0]['nick'],
                'success' => true,
                'data' => $usuario[0]);

        }


    }

    function consultarSesion($session, $nick)
    {
         if ($session->get('user') === null || $session->get('user') !== $nick) {
             return array('mensaje' => "Sesion no valida",
                 'success' => false);
         }
         else{
             return array('success' => true,
                 'user' => $session->get('user'),
                 'pass' => $session->get('pass'));
         }
    }

    function cerrarSesion($session)
    {
        $session->clear();

        return array('mensaje' => "Has cerrado sesion",
            'success' => true);
    }

}