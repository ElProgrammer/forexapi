<?php

/**
 * Created by PhpStorm.
 * User: Xea
 * Date: 5/3/2017
 * Time: 3:31 PM
 */
class ForoController
{

    public function getListaForos()
    {
        $foroDao = new ForoDao();
        $mensajeDao = new MensajeDao();
        $temaDao = new TemaDao();
        $listaForos = $foroDao->obtenerTodos();
        $listaForoDTO = [];
        foreach ($listaForos as $foro) {
            $forodto = new ForoDTO();
            $forodto->setId($foro['id']);
            $forodto->setNombre($foro['nombre']);
            $forodto->setCategoria($foro['categoria']);
            $forodto->setCantidadTemas((int)$temaDao->obtenerCantTemasPorNombreForo($foro['nombre']));
            $forodto->setCantidadMensajes((int)$mensajeDao->obtenerCantMensajesPorNombreForo($foro['nombre']));
            array_push($listaForoDTO, $forodto);
        }
        return $listaForoDTO;

    }
}